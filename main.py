from gevent import monkey
monkey.patch_all()
import os
import signal
import json
from flask import Flask, request, abort, g, url_for
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage
from selenium.common.exceptions import SessionNotCreatedException, WebDriverException, TimeoutException
from scraper import Scraper
import threading
import logging
import redis
from operator import itemgetter
import requests

app = Flask(__name__)
with app.app_context():
    app.extensions['scraper_extension'] = Scraper()
api = LineBotApi(os.environ['LINE_CHANNEL_ACCESS_TOKEN'],timeout=29)
handler = WebhookHandler(os.environ['LINE_CHANNEL_SECRET'])
cache = redis.Redis(host=os.environ['REDIS_HOST'],
    port=os.environ['REDIS_PORT'],password=os.environ['REDIS_PWD'], db=0)

@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    DELIMITER = ' '
    text = event.message.text
    arr_args = text.split(DELIMITER, 1)
    source = None
    if event.source.type == 'user':
        source = event.source.user_id
    elif event.source.type == 'group':
        source = event.source.group_id
    elif event.source.type == 'room':
        source = event.source.room_id
    if arr_args[0] == '!cari':
        if(len(arr_args) > 1):
            try:
                response = threading.Thread(target=push, args=[source,'!cari',str(arr_args[1])])
                response.start()
            except RuntimeError as e:
                logging.exception('Handler.threading')
                api.push_message(source,TextSendMessage(
                    text='Sedang mencari, mohon tunggu sebentar.'))
            except Exception as e:
                logging.exception('Handler.unknown')
                api.push_message(source,TextSendMessage(
                    text='Layanan sedang penuh, silahkan coba beberapa saat lagi.'))
    if arr_args[0] == '!murah':
        if(len(arr_args) > 1):
            try:
                response = threading.Thread(target=push, args=[source,'!murah',str(arr_args[1])])
                response.start()
            except RuntimeError as e:
                logging.exception('Handler.threading')
                api.push_message(source,TextSendMessage(
                    text='Sedang mencari, mohon tunggu sebentar.'))
            except Exception as e:
                logging.exception('Handler.unknown')
                api.push_message(source,TextSendMessage(
                    text='Layanan sedang penuh, silahkan coba beberapa saat lagi.'))
    elif arr_args[0] == '!print':
        try:
            last_cache = cache.get(source)
            if last_cache is None:
                api.push_message(source,TextSendMessage(text='Keranjang belanja anda kosong.'))
            else:
                last_cache = json.loads(str(last_cache,'utf-8'))
                invoice_payload = []
                for product in last_cache['cart']:
                    line_item = {}
                    line_item['name'] = product['title']
                    line_item['quantity'] = product['quantity']
                    line_item['unit_cost'] = product['price']
                    invoice_payload.append(line_item)
                session = requests.Session()
                logo = "https://images2.imgbox.com/c9/09/jLq5Ubfp_o.png"
                data = {
                    "currency":"IDR",
                    "header":" ",
                    "from":" ",
                    "to":source,
                    "logo": logo,
                    "items": invoice_payload,
                    "notes":"Terimakasih sudah mencoba layanan ini!"}
                headers = {"Content-Type":"application/json"}
                generated_invoice = session.post("https://invoice-generator.com",data=json.dumps(data), headers=headers)
                if generated_invoice.status_code == 200:
                    filename = os.path.join(source + '.pdf')
                    os_path = os.path.join(app.root_path, 'static', filename)
                    with open(os_path, "wb") as file:
                        file.write(generated_invoice.content)
                    api.push_message(source,TextSendMessage(text=url_for('static', filename=filename,_external=True)))
                else:
                    logging.exception('Handler.invoicing')
                    api.push_message(source,TextSendMessage(text='Layanan sedang penuh, silahkan coba beberapa saat lagi.'))
        except Exception as e:
            logging.exception('Handler.printing')
            api.push_message(source,TextSendMessage(text='Layanan sedang penuh, silahkan coba beberapa saat lagi.'))
    elif arr_args[0] == '!hapus':
        cache.delete(source)
        api.push_message(source,TextSendMessage(text='Keranja belanja dikosongkan.'))
    elif arr_args[0] == '!lihat':
        last_cache = cache.get(source)
        if last_cache is None:
            api.push_message(source,TextSendMessage(text='Keranjang belanja anda kosong.'))
        else:
            last_cache = json.loads(str(last_cache,'utf-8'))
            total = 0
            text_response = 'Keranjang belanja anda:\n\n'
            for product in last_cache['cart']:
                text_response += product['title']
                text_response += '\n  Rp. '
                text_response += str(product['price'])
                text_response += '   x '
                text_response += str(product['quantity'])
                text_response += ' = Rp. '
                amount = product['price'] * product['quantity']
                total += amount
                text_response += str(amount)
                text_response += '\n  '
                text_response += product['link']
                text_response += '\n'
            text_response += '\nTotal: Rp. '
            text_response += str(total)
            api.push_message(source,TextSendMessage(text=text_response))
    else:
        return('', 204)

def push(source,command,query):
    text_response = ''
    try:
        with app.app_context():
            payload = {}
            text_response = ''
            payload['cart'] = []
            payload['cart'].append(app.extensions['scraper_extension'].scrape(
                    'https://www.tokopedia.com',query))
            payload['cart'].append(app.extensions['scraper_extension'].scrape(
                    'https://www.bukalapak.com',query))
            payload['cart'] = [p for p in payload['cart'] if p is not None]
            if len(payload['cart']) == 0:
                raise TimeoutException('Push.none')
            if command == '!cari':
                text_response = 'Menampilkan semua pencarian:\n\n'
                for product in payload['cart']:
                    text_response += product['title']
                    text_response += '\n  Rp. '
                    text_response += str(product['price'])
                    text_response += '\n  '
                    text_response += product['link']
                    text_response += '\n'
                api.push_message(source,TextSendMessage(text=text_response))
                return
            if command == '!murah':
                text_response = 'Memasukkan produk termurah:\n\n'
                if len(payload['cart']) > 1:
                    payload['cart'] = sorted(payload['cart'], key=itemgetter('price'), reverse=False)
                    payload['cart'] = payload['cart'][:1]
            last_cache = cache.get(source) or b'{"cart":[]}'
            last_cache = json.loads(str(last_cache,'utf-8'))
            for product in payload['cart']:
                found = False
                for cached_product in last_cache['cart']:
                    if product['title'] == cached_product['title']:
                        cached_product['quantity'] += 1
                        found = True
                        break
                if not found:
                    product['quantity'] = 1
                    last_cache['cart'].append(product)
            cache.set(source,json.dumps(last_cache))
            for product in payload['cart']:
                text_response += product['title']
                text_response += '\n  Rp. '
                text_response += str(product['price'])
                text_response += '\n  '
                text_response += product['link']
                text_response += '\n'
            api.push_message(source,TextSendMessage(text=text_response))
    except TimeoutException as e:
        logging.exception('Push.timeout')
        api.push_message(source,TextSendMessage(
            text='Maaf, produk tidak ditemukan.'))
    except WebDriverException as e:
        logging.exception('Push.webdriver')
        api.push_message(source,TextSendMessage(
            text='Sedang mencari, mohon tunggu sebentar.'))
    except Exception as e:
        logging.exception('Push.unknown')
        api.push_message(source,TextSendMessage(
            text='Layanan sedang penuh, silahkan coba beberapa saat lagi.'))

def handle_signal(signum, frame):
    with app.app_context():
        app.extensions['scraper_extension'].teardown()

signal.signal(signal.SIGINT, handle_signal)
signal.signal(signal.SIGTERM, handle_signal)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
