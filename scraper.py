import os
from threading import Lock as TLock
from multiprocessing import Lock as MLock
from flask import current_app
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import SessionNotCreatedException, WebDriverException, TimeoutException
from urllib.request import urlretrieve
from urllib.parse import urlencode
import random
import logging
import time
import re

class DriverPool:
    def __init__(self, name, size, limit):
        self.name = name
        self.size = size
        self.trickle = 1
        self.limit = limit
        self._options = webdriver.ChromeOptions()
        self._options.binary_location = os.environ.get('GOOGLE_CHROME_SHIM')
        self._options.add_argument('--headless')
        self._options.add_argument('--no-sandbox')
        self._options.add_argument('--log-level=3')
        self._options.add_argument('--disable-dev-shm-usage')
        self._options.add_argument('--disable-gpu')
        self._options.add_argument('--disable-gl-drawing-for-tests')
        self._options.add_argument('--disable-default-apps')
        self._options.add_argument('--disable-extensions')
        self._options.add_argument('--disable-popup-blocking')
        self._options.add_argument('--incognito')
        experimental_options ={
            'profile.default_content_settings.cookies': 2,
            'profile.managed_default_content_settings.images':2,
            'profile.default_content_setting_values.notifications':2,}
        self._options.experimental_options['prefs'] = experimental_options
        self._pool = [ self.instantiate() for _ in range(self.size)]
        print(self.name + ': ' + str(len(self._pool)) + ' drivers available')

    def instantiate(self):
        capabilities = DesiredCapabilities().CHROME
        capabilities['pageLoadStrategy'] = 'none'
        driver = webdriver.Chrome('chromedriver',desired_capabilities=capabilities,chrome_options=self._options)
        return driver

    def acquire(self, mlock, tlock):
        if not self._pool:
            if ((self.size + self.trickle) <= self.limit):
                self.lease()
            raise WebDriverException('Driver.acquire')
            return None
        else:
            print(self.name + ': Assigning 1 driver...')
            driver = self._pool.pop()
            return driver

    def recycle(self, driver, handle):
        print(self.name + ': Recycling 1 driver...')
        driver.switch_to.window(handle)
        driver.close()
        driver.switch_to.window(driver.window_handles[0])
        self._pool.append(driver)
        print(self.name + ': ' + str(len(self._pool)) + ' drivers available')

    def lease(self):
        try:
            print(self.name + ': Leasing ' + str(self.size) + ' new drivers...')
            [self._pool.append(self.instantiate()) for _ in range(self.size)]
            print(self.name + ': ' + str(len(self._pool)) + ' drivers available')
        finally:
            self.size = self.size + self.trickle

    def teardown(self):
        print(self.name + ': Tearing down...')
        [driver.close() for driver in self._pool]
        [driver.quit() for driver in self._pool]

class Scraper:
    def __init__(self):
        self.pool_size = 1
        self.pool_limit = 1
        self.tokopedia = DriverPool('Tokopedia',self.pool_size,self.pool_limit)
        self.bukalapak = DriverPool('Bukalapak',self.pool_size,self.pool_limit)
        self._tlock = TLock()
        self._mlock = MLock()

    def scrape(self, website, query):
        if website == 'https://www.tokopedia.com':
            try:
                time.sleep(1)
                driver = self.tokopedia.acquire(self._mlock,self._tlock)
            except Exception as e:
                raise WebDriverException('Scraper.acquire')
                return None
            try:
                params={'ob':'5','st':'product','q':query,'cache_invalidator':random.randint(0, 999),}
                driver.execute_script("window.open('');")
                driver.switch_to.window(driver.window_handles[-1])
                handle = driver.current_window_handle
                driver.get('https://www.tokopedia.com/search?' + urlencode(params))
                wait = WebDriverWait(driver, 14)
                element = wait.until(EC.presence_of_element_located((By.CLASS_NAME,'pcr')))
                product = driver.find_element_by_class_name('pcr')
                title = product.find_element_by_tag_name('h3').get_attribute('textContent')
                price = product.find_element_by_css_selector('span[itemprop="price"]').find_element_by_tag_name('span').get_attribute('textContent')
                price = price.replace('Rp ','')
                price = int(price.replace('.',''))
                link = product.find_element_by_tag_name('a').get_attribute('href')
            except Exception as e:
                return None
            finally:
                try:
                    self.tokopedia.recycle(driver, handle)
                except Exception as e:
                    raise WebDriverException('Scraper.recycle')
                    return None
            return {'title':title,
                'price':price,
                'link':link,}
        elif website == 'https://www.bukalapak.com':
            try:
                time.sleep(1)
                driver = self.bukalapak.acquire(self._mlock,self._tlock)
            except Exception as e:
                raise WebDriverException('Scraper.acquire')
                return None
            try:
                params={'utf8':'✓','search[sort_by]':'weekly_sales_ratio:desc','search[keywords]':query,'cache_invalidator':random.randint(0, 999),}
                driver.execute_script("window.open('');")
                driver.switch_to.window(driver.window_handles[-1])
                handle = driver.current_window_handle
                driver.get('https://www.bukalapak.com/products?' + urlencode(params))
                wait = WebDriverWait(driver, 14)
                element = wait.until(EC.presence_of_element_located((By.CLASS_NAME,'product-description')))
                container = driver.find_element_by_class_name('basic-products')
                product = container.find_element_by_class_name('product-description')
                summary = product.find_element_by_class_name('product__name')
                title = summary.get_attribute('title')
                link = summary.get_attribute('href')
                price = product.find_element_by_class_name('amount').get_attribute('textContent')
                price = int(price.replace('.',''))
            except Exception as e:
                return None
            finally:
                try:
                    self.bukalapak.recycle(driver,handle)
                except Exception as e:
                    raise WebDriverException('Scraper.recycle')
                    return None
            return {'title':title,
                'price':price,
                'link':link,}
        else:
            return None

    def teardown(self):
        self.tokopedia.teardown()
        self.bukalapak.teardown()